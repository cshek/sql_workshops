-- Find all trades by a given trader on a given stock - for example the trader 
-- with ID=1 and the ticker 'MRK'. This involves joining from trader to position 
-- and then to trades twice, through the opening- and closing-trade FKs.
SELECT p.opening_trade_ID, p.closing_trade_ID, tr.instant, tr.stock, tr.buy, tr.size, tr.price FROM trader as t
LEFT JOIN position as p
ON t.id = p.trader_ID
LEFT JOIN trade as tr
ON p.opening_trade_ID = tr.id
LEFT JOIN trade as tr2
ON p.closing_trade_ID = tr2.id
WHERE t.id = 1 AND tr.stock = 'MRK'

-- Find the total profit or loss for a given trader over the day, as the sum 
-- of the product of trade size and price for all sales, minus the sum of the 
-- product of size and price for all buys.
Select t.first_name, t.last_name, b1.sells, b2.buys, b1.sells - b2.buys as profit_loss
from trader t
left join 
(SELECT t.ID, SUM(price * size) as sells from trader t
LEFT JOIN position as p
ON t.ID = p.trader_ID
LEFT JOIN trade tr
ON tr.ID = p.opening_trade_ID or tr.id = p.closing_trade_ID
where buy = 0
GROUP BY t.ID)  as b1 on t.id=b1.Id 
left join 
(SELECT t.ID, SUM(price * size) as buys from trader t
LEFT JOIN position as p
ON t.ID = p.trader_ID
LEFT JOIN trade tr
ON tr.ID = p.opening_trade_ID or tr.id = p.closing_trade_ID
where buy = 1
GROUP BY t.ID) as b2 on t.id=b2.id 

-- Develop a view that shows profit or loss for all traders.
CREATE VIEW TradersProfitAndLoss AS
Select t.first_name, t.last_name, b1.sells, b2.buys, b1.sells - b2.buys as profit_loss
from trader t
left join 
(SELECT t.ID, SUM(price * size) as sells from trader t
LEFT JOIN position as p
ON t.ID = p.trader_ID
LEFT JOIN trade tr
ON tr.ID = p.opening_trade_ID or tr.id = p.closing_trade_ID
where buy = 0
GROUP BY t.ID)  as b1 on t.id=b1.Id 
left join 
(SELECT t.ID, SUM(price * size) as buys from trader t
LEFT JOIN position as p
ON t.ID = p.trader_ID
LEFT JOIN trade tr
ON tr.ID = p.opening_trade_ID or tr.id = p.closing_trade_ID
where buy = 1
GROUP BY t.ID) as b2 on t.id=b2.id 

SELECT * FROM TradersProfitAndLoss