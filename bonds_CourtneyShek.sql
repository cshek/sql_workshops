-- Show all information about the bond with the CUSIP '28717RH95'.
SELECT * FROM bond WHERE cusip = '28717RH95'

-- Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
SELECT * FROM bond ORDER BY maturity

-- Calculate the value of this bond portfolio, as the sum of the product of 
-- each bond's quantity and price.
SELECT SUM(quantity * price) AS value FROM bond

-- Show the annual return for each bond, as the product of the quantity and 
-- the coupon. Note that the coupon rates are quoted as whole percentage points, 
-- so to use them here you will divide their values by 100.
SELECT (quantity * coupon)/100 FROM bond

-- Show bonds only of a certain quality and above, for example those bonds 
-- with ratings at least AA2. (Don't resort to regular-expression or other 
-- string-matching tricks; use the bond-rating ordinal.)
SELECT * FROM bond WHERE rating in ('AAA', 'AAA2', 'AA1')

-- Show the average price and coupon rate for all bonds of each bond rating.
SELECT rating, AVG(price), AVG(coupon) FROM bond GROUP BY rating

-- Calculate the yield for each bond, as the ratio of coupon to price. 
-- Then, identify bonds that we might consider to be overpriced, as those 
-- whose yield is less than the expected yield given the rating of the bond.
SELECT b.rating, coupon / price as yield FROM bond as b
LEFT JOIN (SELECT rating, AVG(coupon/price) as avgyield FROM bond GROUP BY rating) as b2
ON b.rating = b2.rating
WHERE b.coupon/b.price <b2.avgyield